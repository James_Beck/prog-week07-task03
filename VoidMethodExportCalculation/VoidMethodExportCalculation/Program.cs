﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoidMethodExportCalculation
{
    class Program
    {
        static void Main(string[] args)
        {
            calculation();
        }

        static void calculation()
        {
            int number = 0;
            do
            {
                Console.WriteLine("Put in an integer for us that isn't 0 would ya?");
                string input = Console.ReadLine();
                int.TryParse(input, out number);
                Console.Clear();
            } while (number == 0);

            Console.WriteLine("Cool your number is {0}.", number);

            Console.WriteLine("Did you know, {0} + {0} equals {1}", number, number + number);
            Console.WriteLine("Did you know, {0} - {0} equals {1}", number, number - number);
            Console.WriteLine("Did you know, {0} * {0} equals {1}", number, number * number);
            Console.WriteLine("Did you know, {0} / {0} equals {1}", number, number / number);
        }
    }
}
